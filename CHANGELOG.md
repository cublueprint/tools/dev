# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.0.1](https://gitlab.com/cublueprint/tools/dev/compare/v1.1.0...v1.0.1) (2021-10-07)

## 1.0.0 (2021-08-11)


### Features

* add release pipeline (EN-362) ([9f18827](https://gitlab.com/cublueprint/beneficent/cli/commit/9f188274e22cabf94040c701077dd33a8fe78c1f))
* clone repos during setup (EN-363) ([0fc473f](https://gitlab.com/cublueprint/beneficent/cli/commit/0fc473f92e662212a0658f47f38121e4ba9ced41))


### Bug Fixes

* clone repos synchronously (EN-363) ([d0ce7c4](https://gitlab.com/cublueprint/beneficent/cli/commit/d0ce7c4a3518d0731d331f3acb2947cf823c8970))
