#!/usr/bin/env node

const dotenv = require("dotenv");
const boxen = require("boxen");
const chalk = require("chalk");
const { ArgumentParser } = require("argparse");
const prompt = require("prompt-sync")({ sigint: true });
const { version } = require("../package.json");
const commandSchema = require("./command.validation");
const { execCommand, writeEnvVars } = require("./util");

let envVars = require("./envVars.json");

const serveCli = (error, value) => {
  if (error) {
    console.log(error.details.map((details) => details.message).join(", "));
    return -1;
  }

  const isInCorrectDirectory = prompt(`You are currently in ${process.cwd()} is this the root path to the project? y/n: `);
  if (isInCorrectDirectory === "n") {
    process.exit(-1);
  }

  const service = value.service;
  const command = value.command;

  switch (service) {
    case "util":
      utilityCommands(command);
      break;
    case "client":
      clientCommands(command);
      break;
    case "server":
      serverCommands(command);
      break;
    case "git":
      gitCommands(command);
      break;
    default:
      console.error("This command is not supported");
      break;
  }
};

const utilityCommands = (command) => {
  switch (command) {
    case "setup":
      const repos = [
        "https://gitlab.com/cublueprint/beneficent/server.git",
        "https://gitlab.com/cublueprint/beneficent/client.git",
        "https://gitlab.com/cublueprint/beneficent/nginx.git"
      ];

      repos.forEach((repo) => {
        execCommand(`git clone ${repo}`, (sync = true));
      });

      console.log(
        boxen(
          "You can use MongoDB Atlas (ask Hamza for help) or a local database \nVisit https://docs.mongodb.com/guides/server/install/" +
            " for guide on how to install it locally\n\nWindows guide for local setup: https://www.youtube.com/watch?v=wcx3f0eUiAw\n" +
            "MacOS guide for local setup: https://www.youtube.com/watch?v=cu9-GphZkKI",
          { padding: 1 }
        )
      );

      envVars.MONGODB_URL = prompt("Please enter the dev database URI (eg: mongodb://localhost:27017/db-dev): ");
      envVars.MONGODB_TEST_URL = prompt("Please enter the test database URI (eg: mongodb://localhost:27017/db-test): ");

      console.log(
        boxen(
          "1) Go to https://ethereal.email/create\n2) Click the create button\n3) Enter the Ethereal username and password below",
          { padding: 1 }
        )
      );

      envVars.SMTP_USERNAME = prompt("Ethereal username: ");
      envVars.SMTP_PASSWORD = prompt("Ethereal password: ");

      writeEnvVars(envVars);
      console.log(chalk.green.bold("Envoirnment variables have been saved!"));

      console.log(chalk.white.bold("Installing client modules now, this might take a while"));
      execCommand("yarn --cwd ./client install", (sync = true));
      console.log(chalk.green.bold("Done installing client modules!"));
      console.log(chalk.white.bold("Installing server modules now, this might take a while"));
      execCommand("yarn --cwd ./server install", (sync = true));
      console.log(chalk.green.bold("Done installing server modules!"));
      break;

    case "clean":
      console.log(chalk.white.bold("Removing node_modules folders in client and server services, this might take a while"));
      execCommand("rm -rf ./client/node_modules");
      execCommand("rm -rf ./server/node_modules");
      console.log(chalk.white.bold("Clearing the yarn cache, this might take a while"));
      execCommand("yarn cache clean");
      break;
    case "migrate":
      execCommand("node ./server/src/utils/migrateCSV.js ./server/src/utils/test.csv");
      break;
    case "seed":
      execCommand(`yarn --cwd ./server seed`);
      break;
    default:
      console.error("This command is not supported");
      break;
  }
};

const clientCommands = (command) => {
  switch (command) {
    case "start":
      process.env["NODE_ENV"] = "development";
      process.env["PORT"] = 3007;
      process.env["REACT_APP_API_ENDPOINT"] = "http://localhost:5001";
      execCommand(`yarn --cwd ./client start`);
      break;
    case "test":
      process.env["NODE_ENV"] = "test";
      execCommand(`yarn --cwd ./client test --runInBand --watchAll=false`);
      break;
    case "coverage":
      process.env["NODE_ENV"] = "test";
      execCommand(`yarn --cwd ./client test --runInBand --watchAll=false --coverage`);
      break;
    case "update-snapshots":
      process.env["NODE_ENV"] = "test";
      execCommand(`yarn --cwd ./client test -u  --runInBand --watchAll=false`);
      break;
    case "style:check":
      execCommand(`yarn --cwd ./client style:check`);
      break;
    case "style:fix":
      execCommand(`yarn --cwd ./client style:fix`);
      break;
    default:
      break;
  }
};

const serverCommands = (command) => {
  switch (command) {
    case "start":
      process.env["NODE_ENV"] = "development";
      process.env["PORT"] = 5001;
      execCommand(`yarn --cwd ./server start:nodemon`);
      break;
    case "debug":
      process.env["NODE_ENV"] = "development";
      execCommand(`yarn --cwd ./server debug`);
      break;
    case "test":
      process.env["NODE_ENV"] = "test";
      execCommand(`yarn --cwd ./server test --runInBand`);
      break;
    case "coverage":
      process.env["NODE_ENV"] = "test";
      execCommand(`yarn --cwd ./server test --runInBand --coverage`);
      break;
    case "style:check":
      execCommand(`yarn --cwd ./server style:check`);
      break;
    case "style:fix":
      execCommand(`yarn --cwd ./server style:fix`);
      break;
    default:
      console.error("This command is not supported");
      break;
  }
};

const gitCommands = (command) => {
  switch (command) {
    case "commit":
      console.log(
        boxen(
          "Which of the following best describes the changes in your commit: \n1) Feature (new feature)\n2) Fix (fixing an existing feature)\n3) Chore (there is no associated Jira task for these changes)",
          {
            padding: 1
          }
        )
      );
      const typeOfCommit = prompt("Please enter 1, 2 or 3: ").trim();
      if (!["1", "2", "3"].includes(typeOfCommit)) {
        console.error("This command is not supported");
        break;
      }
      const isBreakingChange = prompt("Is your commit a breaking change? (y/n): ");
      const commitMessage = prompt("Please enter the commit message (no need to mention 'feat/fix/chore' here): ")
        .toLowerCase()
        .trim();
      let jiraTaskID = "";
      if (typeOfCommit !== "3") {
        jiraTaskID = prompt("Please enter the Jira Task ID (eg. EN-123): ").trim();
      }
      switch (typeOfCommit) {
        case "1":
          execCommand(`git commit -m "feat${isBreakingChange === "y" ? "!" : ""}: ${commitMessage} (${jiraTaskID})"`);
          break;
        case "2":
          execCommand(`git commit -m "fix${isBreakingChange === "y" ? "!" : ""}: ${commitMessage} (${jiraTaskID})"`);
          break;
        case "3":
          execCommand(`git commit -m "chore: ${commitMessage}"`);
          break;
      }
      break;
    default:
      console.error("This command is not supported");
      break;
  }
};

dotenv.config();

const parser = new ArgumentParser({
  usage:
    `\n  dev ${chalk.yellow("<service>")} ${chalk.blue("<command>")}\n\n` +
    "documentation:\n" +
    `  dev ${chalk.yellow("util")} ${chalk.blue("<setup, seed, clean, migrate>")}\n` +
    `  dev ${chalk.yellow("client")} ${chalk.blue("<start, test, coverage, style:check, style:fix, update-snapshots>")}\n` +
    `  dev ${chalk.yellow("server")} ${chalk.blue("<start, debug, test, coverage, style:check, style:fix>")}\n` +
    `  dev ${chalk.yellow("git")} ${chalk.blue("<commit>")}\n\n` +
    "examples:\n" +
    `  dev ${chalk.yellow("setup")} ${chalk.blue("all")}\n` +
    `  dev ${chalk.yellow("server")} ${chalk.blue("start")}\n` +
    `  dev ${chalk.yellow("client")} ${chalk.blue("update-snapshots")}\n` +
    `  dev ${chalk.yellow("git")} ${chalk.blue("commit")}\n`
});

parser.add_argument("-v", "--version", { action: "version", version });
parser.add_argument("service");
parser.add_argument("command");
const args = parser.parse_args();

const command = {
  service: args.service,
  command: args.command
};
const { error, value } = commandSchema.validate(command);

serveCli(error, value);
